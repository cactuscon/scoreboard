request = require 'request'
_       = require 'lodash'
debug   = require('debug')('scoreboard:scoreboard')
MeshbluHttp = require 'meshblu-http'
MeshbluConfig = require 'meshblu-config'

meshbluConfig = new MeshbluConfig().toJSON()

class Scoreboard
  score: (req, res) =>
    scores = new Object()
    meshblu = new MeshbluHttp meshbluConfig
    meshblu.device meshbluConfig.uuid, (error, device) =>
      return callback error if error?
      debug 'getClient found device', device
      scores.white = device?.whiteCount
      scores.black = device?.blackCount
      # callback null, client_id: clientId, client_secret: clientSecret, redirectUri: device.options?.callbackUrl
      res.send(JSON.stringify(scores))

module.exports = Scoreboard
